#!/usr/bin/env python

###########################################################
#   Script to produce parton-level truth distributions    #
###########################################################

# -*- coding: utf-8 -*-
import os, sys
from ROOT import gROOT, TFile, TTree, TBranch, TLorentzVector, Math, TCanvas, TCut, TH1F, TH2F, gROOT, TChain, TMath
import time
#from array import array
#from matplotlib import pyplot as plt
import numpy as np
import math
from optparse import OptionParser

from ToolsForTruth import tools
from MsgHelper import msgServer
from ConfigFile import configFile

# =================================================================================
#  main
# =================================================================================
def main(argv):
    parser = OptionParser()
    parser.add_option("-d", "--debugLevel", dest="debugLevel", default=1, type="int",
                      help="set debug level (DEBUG=0, INFO=1, WARNING=2, ERROR=3, FATAL=4) [default: %default]", metavar="LEVEL")  
    parser.add_option("-i", "--input", dest="myinput", default="",
                      help="set input file or directory or dataset [default: %default]", metavar="FILE or DIR or DATASET")
    parser.add_option("-c", "--configuration", dest="myconfig", default="./config.yaml",
                      help="set input yaml configuration file [default: %default]", metavar="FILE")
    parser.add_option("-f", "--maxfiles", dest="maxfiles", default=-1,
                      help="set number of maximum files to be processed", metavar="MAXFILES", type="int")
    parser.add_option("-e", "--maxevents", dest="maxevents", default=-1,
                      help="set number of maximum events to be processed", metavar="MAXEVENTS", type="int")
    parser.add_option("-s","--stats", action="store_true", dest="showStats",
                          help="show stats box")
    parser.add_option("--interactive", action="store_true", dest="interactive",
                          help="run 4TruthHistos interactively")
    parser.add_option("--normlumi", action="store_true", dest="normlumi",
                          help="normalize histograms to the Run 2 luminosity (i.e. 139 fb-1)")
    parser.add_option("--nonorm", action="store_true", dest="nonorm",
                          help="No normalize histograms at all")
    parser.add_option("--outputformat", dest="outputformat", default="all",
                        help="set output format of plots [default: %default] [options: png, pdf, root, all]", metavar="FORMAT")

    # Convertir channel en lo de e/mu
    #parser.add_option("-c", "--channel", dest="channel", default="",
    #                  help="Select the channel", metavar="CHANNEL")

    try:
        (options, args) = parser.parse_args()
    except:
        parser.print_help()
        exit()

    global msg
    msg = msgServer('4TruthHistos', options.debugLevel)
    msg.printBold('Running 4TruthHistos.py')

    global config
    config = configFile(options)

    global tool
    tool = tools(options, config)

    gROOT.Reset()
    if options.interactive: gROOT.SetBatch(0)
    else: gROOT.SetBatch(1)
    
    start_time = time.time()

    # Selecting the root file(s)
    # Merge chains from all input ROOT files 
    Truth = TChain(config.TreeName)
    for ifile in config.rootFile: Truth.Add(ifile)
    msg.printInfo('- Total entries = %d' % Truth.GetEntries())
    
    #print tree (for debug purposes only)
    #Truth.Print()
    #exit()
   
    style_name = tool.StyleATLAS(0, options.showStats)
    gROOT.SetStyle("Plain")
    gROOT.SetStyle("style_name")


    #########   JUST FOR TEST ::::    DETELE FROM HERE ####
    """
    debugSample = True
    if debugSample == True:
        H_Channels = TH1F('Hdecay', 'Hdecay', 60, -30., 30.)
        W_Channels = TH1F('Wchild1', 'Wchild1', 60, -30., 30.)
        tau_Channels = TH1F('Tau_child1', 'Tau_child1', 60, -30., 30.)
        Z_Channels = TH1F('Zchild1', 'Zchild1', 60, -30., 30.)
        top_lp_decay = TH1F('Top_decay', 'Top_decay' , 60, -30., 30.)
        top_nu_decay = TH1F('Top_nu_decay', 'Top_nu_decay' , 60, -30., 30.)
        ww = 0
        tt = 0
        zz = 0
        gammagamma = 0
        bb = 0
        gg = 0
        cc = 0
        Zgamma = 0
        ss = 0
        mumu = 0
        
        ww_e = 0
        ww_mu = 0
        ww_tau_e = 0
        ww_tau_mu = 0
        ww_tauhad = 0
        ww_had = 0
        ww_9999 = 0
        ww_1845523456 = 0
        ww_1627419749 = 0
        ww_else = 0
        tt_e = 0
        tt_mu = 0
        tt_had = 0
        tt_else = 0
        tt_9999 = 0
        zz_ee = 0
        zz_mumu = 0
        zz_tautau = 0
        zz_et = 0
        zz_mt = 0
        zz_em = 0
        zz_nunu = 0
        zz_hadhad = 0
        zz_9999 = 0
        zz_else = 0
        top_e = 0
        top_mu = 0
        top_TauHad = 0
        top_had = 0
        top_else = 0

        
        _3L1HadTau_tt = 0
        _3L1HadTau_ww = 0
        _3L1HadTau_zz = 0
       
        _3L_tt = 0
        _3L_ww = 0
        _3L_zz = 0

        _s3L1HadTau_tt = 0
        _s3L1HadTau_ww = 0
        _s3L1HadTau_zz = 0
        _s3L1HadTau_tot = 0
       
        _s3L_tt = 0
        _s3L_ww = 0
        _s3L_zz = 0
        _s3L_tot = 0

        stt = 0
        sww = 0
        szz = 0
        
 
        n_events = 0
        for event in Truth:
            if n_events > 1000000:
                break
            n_events = n_events + 1

            
            
            aux = abs(event.MC_Higgs_decay1_pdgId) + abs(event.MC_Higgs_decay1_pdgId)
            if aux == 48:  # WW channel
                ww = ww + event.weight_mc
                sww += pow(event.weight_mc,2)
            if aux == 30:  # tautau channel
                tt = tt + event.weight_mc
                stt += pow(event.weight_mc,2)
            if aux == 46:  # ZZ channel                                                                 
                zz = zz + event.weight_mc
                szz += pow(event.weight_mc,2)
            if aux == 44:  # gammagamma
                gammagamma = gammagamma + event.weight_mc
            if aux == 10:  # bb
                bb = bb + event.weight_mc
            if aux == 42:  # gg
                gg = gg + event.weight_mc
            if aux == 8:  # gcc
                cc = cc + event.weight_mc
            if aux == 45:  # Zgamma
                Zgamma = Zgamma + event.weight_mc
            if aux == 6:  # ss channel                                                                 
                ss = ss + event.weight_mc
            if aux == 26:  # mumu channel                                                                 
                mumu = mumu + event.weight_mc   
                

            if tool.Region(event, '3L1HadTau'):
                if tool.channel == 'Htautau':
                    _3L1HadTau_tt = _3L1HadTau_tt + event.weight_mc
                    _s3L1HadTau_tt += pow(event.weight_mc,2)
                    _s3L1HadTau_tot += pow(event.weight_mc,2)
                if tool.channel == 'HWW':
                    _3L1HadTau_ww = _3L1HadTau_ww + event.weight_mc
                    _s3L1HadTau_ww += pow(event.weight_mc,2)
                    _s3L1HadTau_tot += pow(event.weight_mc,2)
                if str(tool.channel) in  ['HZZ', 'HZZ1', 'HZZ2']:
                    _3L1HadTau_zz = _3L1HadTau_zz  + event.weight_mc
                    _s3L1HadTau_zz += pow(event.weight_mc,2)
                    _s3L1HadTau_tot += pow(event.weight_mc,2)


            if tool.Region(event, '3L'):
                if tool.channel == 'Htautau':
                    _3L_tt = _3L_tt + event.weight_mc
                    _s3L_tt += pow(event.weight_mc,2)
                    _s3L_tot += pow(event.weight_mc,2)
                if tool.channel == 'HWW':
                    _3L_ww = _3L_ww + event.weight_mc
                    _s3L_ww += pow(event.weight_mc,2)
                    _s3L_tot += pow(event.weight_mc,2)
                if str(tool.channel) in  ['HZZ', 'HZZ1', 'HZZ2']:
                    _3L_zz = _3L_zz  + event.weight_mc
                    _s3L_zz += pow(event.weight_mc,2)
                    _s3L_tot += pow(event.weight_mc,2)
                    
            if aux == 30: # study of tau decay
                if abs(event.MC_Higgs_tau_decay1_isHadronic) == 11: 
                    tt_e = tt_e + event.weight_mc
                if abs(event.MC_Higgs_tau_decay2_isHadronic) == 11:
                    tt_e = tt_e+ event.weight_mc
                if abs(event.MC_Higgs_tau_decay1_isHadronic) == 13:
                    tt_mu = tt_mu + event.weight_mc
                if abs(event.MC_Higgs_tau_decay2_isHadronic) == 13:
                    tt_mu = tt_mu + event.weight_mc
                if abs(event.MC_Higgs_tau_decay1_isHadronic) == 24:
                    tt_had = tt_had + event.weight_mc
                if abs(event.MC_Higgs_tau_decay2_isHadronic) == 24:
                    tt_had = tt_had + event.weight_mc

            if aux == 48:
                if abs(event.MC_Higgs_decay1_from_decay1_pdgId) == 11:
                    ww_e = ww_e + event.weight_mc
                if abs(event.MC_Higgs_decay1_from_decay2_pdgId) == 11:
                    ww_e = ww_e + event.weight_mc
                if abs(event.MC_Higgs_decay1_from_decay1_pdgId) == 13:
                    ww_mu = ww_mu + event.weight_mc
                if abs(event.MC_Higgs_decay1_from_decay2_pdgId) == 13:
                    ww_mu = ww_mu + event.weight_mc
                if abs(event.MC_Higgs_decay1_from_decay1_pdgId) == 15:
                    if abs(event.MC_Higgs_tau_decay1_from_decay1_isHadronic) == 11:
                        ww_tau_e = ww_tau_e + event.weight_mc
                    if abs(event.MC_Higgs_tau_decay1_from_decay1_isHadronic) == 13:
                        ww_tau_mu = ww_tau_mu + event.weight_mc
                    if abs(event.MC_Higgs_tau_decay1_from_decay1_isHadronic) == 24:
                        ww_tauhad = ww_tauhad + event.weight_mc
                if abs(event.MC_Higgs_decay1_from_decay2_pdgId) == 15:
                    if abs(event.MC_Higgs_tau_decay1_from_decay2_isHadronic) == 11:
                        ww_tau_e = ww_tau_e + event.weight_mc
                    if abs(event.MC_Higgs_tau_decay1_from_decay2_isHadronic) ==13:
                        ww_tau_mu = ww_tau_mu + event.weight_mc
                    if abs(event.MC_Higgs_tau_decay1_from_decay2_isHadronic) ==24:
                        ww_tauhad = ww_tauhad +event.weight_mc
                if abs(event.MC_Higgs_decay1_from_decay1_pdgId) not in [11, 13, 15]:
                    ww_had = ww_had + event.weight_mc
                if abs(event.MC_Higgs_decay1_from_decay2_pdgId)not in [11, 13, 15]:
                    ww_had = ww_had + event.weight_mc

        msg.printInfo("Higgs decay channels")
        tot1 = ww + tt + zz
        tot = ww + tt + zz + gammagamma + bb + gg + cc + Zgamma + ss + mumu
        msg.printInfo(" H -> WW            ::  {:,.4f} ".format(ww) + "  ( {:,.4f} ".format(100.0*ww/tot) +"%)")
        msg.printInfo(" H -> tau tau       ::  {:,.4f} ".format(tt) + "  ( {:,.4f} ".format(100.0*tt/tot) +"%)")
        msg.printInfo(" H -> ZZ            ::  {:,.4f} ".format(zz) + "  ( {:,.4f} ".format(100.0*zz/tot) +"%)")
        msg.printInfo(" H -> Gamma Gamma   ::  {:,.4f} ".format(gammagamma) + "  ( {:,.4f} ".format(100.0*gammagamma/tot) +"%)")
        msg.printInfo(" H -> bb            ::  {:,.4f} ".format(bb) + "  ( {:,.4f} " .format(100.0*bb/tot) +"%)")
        msg.printInfo(" H -> gg            ::  {:,.4f} ".format(gg) + "  ( {:,.4f} " .format(100.0*gg/tot) +"%)")
        msg.printInfo(" H -> cc            ::  {:,.4f} ".format(cc) + "  ( {:,.4f} " .format(100.0*cc/tot) +"%)")
        msg.printInfo(" H -> Z Gamma       ::  {:,.4f} ".format(Zgamma) + "  ( {:,.4f} " .format(100.0*Zgamma/tot) +"%)")
        msg.printInfo(" H -> ss            ::  {:,.4f} ".format(ss) + "  ( {:,.4f} ".format(100.0*ss/tot) +"%)")
        msg.printInfo(" H -> mu mu         ::  {:,.4f} ".format(mumu) + "  ( {:,.4f} ".format(100.0*mumu/tot) +"%)")
 

        msg.printInfo("Tau decay channels in H-> TauTau")
        tt_total =  tt_e + tt_mu + tt_had
        msg.printInfo(" Tau -> e        :: {:,.4f} ".format(tt_e)  + "  ( {:,.4f} ".format(100.0*tt_e/tt_total) +"%)")
        msg.printInfo(" Tau -> mu       :: {:,.4f} ".format(tt_mu) + "  ( {:,.4f} ".format(100.0*tt_mu/tt_total) +"%)")
        #msg.printInfo(" Tau -> tau_e    :: " + str(tt_tau_e)   + "  ("+ str(100.0*tt_tau_e/tt_total) +"%)")
        #msg.printInfo(" Tau -> tau_mu   :: " + str(tt_tau_mu)  + "  ("+ str(100.0*tt_tau_mu/tt_total) +"%)")
        #msg.printInfo(" Tau -> tau_had  :: " + str(tt_tau_had) + "  ("+ str(100.0*tt_tau_had/tt_total) +"%)")
        msg.printInfo(" Tau -> had      :: {:,.4f} ".format(tt_had)+ "  ( {:,.4f} ".format(100.0*tt_had/tt_total) +"%)")

        msg.printInfo("W decay channels in H->WW")
        ww_total = ww_e + ww_mu + ww_tau_e + ww_tau_mu + ww_had + ww_tauhad
        msg.printInfo(" W -> e         :: {:,.4f} ".format(ww_e)       + "  ( {:,.4f} ".format(100.0*ww_e/ww_total) +"%)")
        msg.printInfo(" W -> mu        :: {:,.4f} ".format(ww_mu)      + "  ( {:,.4f} ".format(100.0*ww_mu/ww_total) +"%)")
        msg.printInfo(" W -> tau -> e  :: {:,.4f} ".format(ww_tau_e)   + "  ( {:,.4f} ".format(100.0*ww_tau_e/ww_total) +"%)")
        msg.printInfo(" W -> tau -> mu :: {:,.4f} ".format(ww_tau_mu)  + "  ( {:,.4f} ".format(100.0*ww_tau_mu/ww_total) +"%)")
        msg.printInfo(" W -> tau - had :: {:,.4f} ".format(ww_tauhad)  + "  ( {:,.4f} ".format(100.0*ww_tauhad/ww_total) +"%)")
        msg.printInfo(" W -> hads      :: {:,.4f} ".format(ww_had)     + "  ( {:,.4f} ".format(100.0*ww_had/ww_total) +"%)")

        msg.printInfo("")
        msg.printInfo("")
        msg.printInfo("Region 2Lep + 1HadTau")
        tot_3L1HadTau = _3L1HadTau_tt + _3L1HadTau_ww + _3L1HadTau_zz
        msg.printInfo(" 2L+1HadTau: H -> tau tau  {:,.4f} :: ".format(_3L1HadTau_tt) + "  ( {:,.4f} ".format(100.0*_3L1HadTau_tt/tot_3L1HadTau) +"%)")
        msg.printInfo(" 2L+1HadTau: H -> WW       {:,.4f} :: ".format(_3L1HadTau_ww) + "  ( {:,.4f} ".format(100.0*_3L1HadTau_ww/tot_3L1HadTau) +"%)")
        msg.printInfo(" 2L+1HadTau: H -> ZZ       {:,.4f} :: ".format(_3L1HadTau_zz) + "  ( {:,.4f} ".format(100.0*_3L1HadTau_zz/tot_3L1HadTau) +"%)")

        _s3L1HadTau_tt_rel = _3L1HadTau_tt/tt * math.sqrt ( _s3L1HadTau_tt/pow(_3L1HadTau_tt,2) + stt/pow(tt,2) )
        _s3L1HadTau_ww_rel = _3L1HadTau_ww/ww * math.sqrt ( _s3L1HadTau_ww/pow(_3L1HadTau_ww,2) + stt/pow(ww,2) )
        _s3L1HadTau_zz_rel = _3L1HadTau_zz/zz * math.sqrt ( _s3L1HadTau_zz/pow(_3L1HadTau_zz,2) + stt/pow(zz,2) )
        
        
        msg.printInfo("")
        msg.printInfo( " Signal events in channel VS Total events in same channel")
        msg.printInfo( " H->TauTau(2L+1HadTau) / H->TauTau(Total) =  {:,.4f} ".format(_3L1HadTau_tt/tt)+" +- {:,.4f}".format(_s3L1HadTau_tt_rel))
        msg.printInfo( " H-> W W (2L+1HadTau)  / H-> W W (Total)  =  {:,.4f} ".format(_3L1HadTau_ww/ww)+" +- {:,.4f}".format(_s3L1HadTau_ww_rel))
        msg.printInfo( " H-> Z Z (2L+1HadTau)  / H-> Z Z (Total)  =  {:,.4f} ".format(_3L1HadTau_zz/zz)+" +- {:,.4f}".format(_s3L1HadTau_zz_rel))


        msg.printInfo("")
        msg.printInfo("")
        msg.printInfo("Region 3L  (e/mu)")
        tot_3L = _3L_tt + _3L_ww + _3L_zz
        msg.printInfo(" 3L: H -> tau tau  :: {:,.4f} ".format(_3L_tt) + "  ( {:,.4f} ".format(100.0*_3L_tt/tot_3L) +"%)")
        msg.printInfo(" 3L: H -> WW       :: {:,.4f} ".format(_3L_ww) + "  ( {:,.4f} ".format(100.0*_3L_ww/tot_3L) +"%)")
        msg.printInfo(" 3L: H -> ZZ       :: {:,.4f} ".format(_3L_zz) + "  ( {:,.4f} ".format(100.0*_3L_zz/tot_3L) +"%)")

        
        _s3L_tt_rel = _3L_tt/tt * math.sqrt ( _s3L_tt/pow(_3L_tt,2) + stt/pow(tt,2) )
        _s3L_ww_rel = _3L_ww/ww * math.sqrt ( _s3L_ww/pow(_3L_ww,2) + stt/pow(ww,2) )
        _s3L_zz_rel = _3L_zz/zz * math.sqrt ( _s3L_zz/pow(_3L_zz,2) + stt/pow(zz,2) )
        
        
        msg.printInfo("")
        msg.printInfo( " Signal events in channel VS Total events in same channel")
        msg.printInfo( " H->TauTau(3L) / H->TauTau(Total) =  {:,.4f} ".format(_3L_tt/tt)+" +- {:,.4f}".format(_s3L_tt_rel))
        msg.printInfo( " H-> W W (3L)  / H-> W W (Total)  =  {:,.4f} ".format(_3L_ww/ww)+" +- {:,.4f}".format(_s3L_ww_rel))
        msg.printInfo( " H-> Z Z (3L)  / H-> Z Z (Total)  =  {:,.4f} ".format(_3L_zz/zz)+" +- {:,.4f}".format(_s3L_zz_rel))


     """
        ####exit() 
	#########   JUST FOR TEST ::::    DETELE UNTIL HERE ####


    # Activate only the necessary branches
    tool.SpeedBranches(Truth)
	    
    # signal regions
    SRs = ['3L1HadTau', '3L']
    histosInSR = {}
    SpecificHistos = {}

    print "SRs before calling DefBasic1DHistos"
    print SRs
        

    
    # Step 1 - Declaration
    msg.printGreen("Step 1 - Defining the histograms")
    msg.printInfo("- Defining basic histos...")    
    tool.DefBasic1DHistos(histosInSR, SRs)

    # additional 1D histos
    msg.printInfo("- Defining additional 1D histos...")
    tool.DefSpecificHistos(SpecificHistos)


    # print histosInSR

    print "SRs after calling DefBasic1DHistos"
    print SRs
        


    # Step 2 - Filling
    msg.printGreen("Step 2 - Loop over events to fill the histograms...")
    start_fillAll = time.time()

    # initialize counter for all processed events
    j1 = 0

    # initialize counter for events passing each region
    j2 = {}
    j2["all"] = 0
    for region in SRs: j2[region] = 0

    # set maximum for the progress bar
    progressBarMaxEvents = Truth.GetEntries()
    if options.maxevents > 0: progressBarMaxEvents = options.maxevents

        # event loop
    for event in Truth:
        if options.maxevents > 0 and j1 == options.maxevents: break
        j1 = j1+1

        # Progress bar
        if msg.debugLevel > 0:
            sys.stdout.flush()
            sys.stdout.write("\r- Events : {0:.1f} ".format(float(j1)/float(progressBarMaxEvents)*100.)+"%")
            sys.stdout.flush()
        else:
            msg.printDebug("===============================================")
            msg.printDebug(" Event %d" % j1)
            msg.printDebug("===============================================")
            
        if tool.normalize == 'lumi': self.customLabel(labelPos[0], labelPos[1]-0.05, "#sqrt{s} = 13 TeV, 139 fb^{#minus1}")
        elif tool.normalize == 'entries': sum_step ='event.weight_mc'
        elif tool.normalize == 'nonorm': sum_step = '1'
        else: sum_step = 1

        # Fill common histos (all events)
        # tool.FillBasic1DCommonHistos(event, histosInSR)

        tool.AnalyseSpecificRegion(event, SpecificHistos)

        # selection
        selectionPerRegion = []

        for region in SRs:
            #print 'region',region
            selectionPerRegion += [ tool.Region(event, region) ]
            if tool.Region(event, region): j2[region] = j2[region] + eval(sum_step)
        if not any(selectionPerRegion): continue
        

        msg.printDebug("- Event accepted!!!")

        # Fill basic histos per signal region
        tool.FillBasic1DHistos(event, histosInSR)                  # appear explicitly in the tree
        # tool.FillChargeAndMtHistos(event, ChargeHistos, MtHistos)  # M_T and charge for particles with pdgId info in Tree 
        
        j2["all"] = j2["all"] + eval(sum_step)



    sys.stdout.write('\n')
    #msg.printInfo("Processed events: %2.1f%% [%d/%d]" % (100*float(j1)/float(Truth.GetEntries()), j1, Truth.GetEntries()))
    msg.printInfo("Events that pass all regions: %d" % j2["all"])
    for region in SRs:
        msg.printInfo("- Events that pass region %s: %d" % (region, j2[region]))

    
    # Step 3 - Drawing
    msg.printGreen("Step 3 - Drawing and saving the histograms")
    start_drawAll = time.time()

    # draw plots for pdgIds
    # tool.DrawDonutsForHiggsDecay(histosInSR)
    # tool.DrawDonutsForPDGIds(histosInSR)
    
    # Histos - Basic
    tool.DrawBasic1DHistos(histosInSR)

    tool.DrawOneBasic(SpecificHistos[0], '3L1HadTau', ["Top Mass", 100., 100., 300., "M_{Top}", "GeV"],"TopMass")
    tool.DrawOneBasic(SpecificHistos[1], '3L1HadTau', ["W from top Mass", 100., 50., 150., "M_{WTop}", "GeV"],"WTopMass")
    tool.DrawOneBasic(SpecificHistos[2], '3L1HadTau', ["Higgs Mass", 100., 0., 200., "M_{H}", "GeV"],"HigssMass")
    tool.DrawOneBasic(SpecificHistos[3], '3L1HadTau', ["Higgs Mass", 100., 0., 200., "M_{H#rightarrow#tau#tau}", "GeV"],"HigssMassTauTau")
    tool.DrawOneBasic(SpecificHistos[4], '3L1HadTau', ["Higgs Mass", 100., 0., 200., "M_{H#rightarrow#tau#tau}", "GeV"],"HigssMassTauVisTauVis")

    
    tool.Draw2DHisto(SpecificHistos[10], '3L1HadTau','SCAT',['pt_MC_secondb_afterFSR_MC_b_from_t','p_{T}(b from t) [GeV]', 'p_{T}(secondb after FSR) [GeV]'])
    tool.Draw2DHisto(SpecificHistos[11], '3L1HadTau','SCAT',['eta_MC_secondb_afterFSR_MC_b_from_t','#eta(b from t)', '#eta(secondb after FSR)'])

    end_drawAll = time.time()
    msg.printInfo("Drawing time: " +str((end_drawAll - start_drawAll)/60) + " min")
    # End drawing

    
    msg.printInfo("--- %s minutes ---" % ((time.time() - start_time)/60))


    

# =================================================================================
#  __main__
# =================================================================================
if __name__ == '__main__':
  main(sys.argv[1:])
