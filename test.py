from ROOT import gROOT, TCanvas, TH1, TH1F

gROOT.Reset()
gROOT.SetBatch(0)

c = TCanvas("c", "canvas", 800, 800)
c.cd()

TH1.SetDefaultSumw2()
h1 = TH1F("h1", "h1", 100, -5, 5)
h1.SetLineColor(1)
h1.SetLineWidth(2)
h1.FillRandom("gaus")
h1.Draw("HIST")

h1_clone = h1.Clone("h1_clone")
h1_clone.SetMarkerSize(0)
h1_clone.SetFillColor(1)
h1_clone.SetFillStyle(3256)
h1_clone.Draw("E2 SAME")

c.Update()

try: input("Press enter to continue")
except SyntaxError: pass
